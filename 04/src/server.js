const express = require('express');
const app = express();
const redis = require('redis');

const PORT = 8080;

const client = redis.createClient();

client.on('connect', function() {
    console.log('Redis client connected');
});

client.on('error', function (err) {
    console.log('Something went wrong ' + err);
});

app.get('/person/:name/address', (req, res) =>
{
    console.log(req.params.name);

    client.get(req.params.name, (error, result) =>
    {
        if (error)
        {
            console.log(error);
            throw error;
        }
        // Log result
        console.log('GET result ->' + result);
        // Print result into receiver (web/terminal...)
        res.send(result+'\n');
    });
});

app.listen(PORT, () =>
{
    console.log('Server listening on port ' + PORT + '...');
    // Add testing key-value pair into redis
    client.set('John', 'Thakurova 9, 160 00, Prague', redis.print);
});
