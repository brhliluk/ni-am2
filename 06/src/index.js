const HTTP2_PORT = 6406;

const fs = require("fs");
const mime = require("mime");
const http2 = require("http2");

const serverOptions = {
    key: fs.readFileSync(__dirname + "/localhost-privkey.pem"),
    cert: fs.readFileSync(__dirname + "/localhost-cert.pem")
};

/**
 * create an http2 server
 */

// read and send file content in the stream
const sendFile = (stream, fileName) => {
    const fd = fs.openSync(fileName, "r");
    const stat = fs.fstatSync(fd);
    const headers = {
        "content-length": stat.size,
        "last-modified": stat.mtime.toUTCString(),
        "content-type": mime.getType(fileName)
    };
    stream.respondWithFD(fd, headers);
    stream.on("close", () => {
        console.log("closing file", fileName);
        fs.closeSync(fd);
    });
    stream.end();
};

// handle requests
const http2Handlers = (req, res) => {
    console.log(req.url);
    if (req.url === "/") {
        // push style.css
        pushFile(res.stream, "/style.css", "style.css");

        // push script
        const files = fs.readdirSync(__dirname + "/scripts");
        pushFile(res.stream, "/scripts/" + files[0], __dirname + "/scripts/" + files[0])

        // push image
        const imageFiles = fs.readdirSync(__dirname + "/images");
        pushFile(res.stream, "/images/" + imageFiles[0], __dirname + "/images/" + imageFiles[0])

        // send index.html file
        sendFile(res.stream, "index.html");
    } else {
        const fileName = __dirname + req.url;
        sendFile(res.stream, fileName);
    }
};

const pushFile = (stream, path, fileName) => {
    stream.pushStream({":path": path}, (err, pushStream) => {
        if (err) {
            throw err;
        }
        sendFile(pushStream, fileName);
    });
};

http2.createSecureServer(serverOptions, http2Handlers).listen(HTTP2_PORT, () => {
    console.log("http2 server started on port", HTTP2_PORT);
});
