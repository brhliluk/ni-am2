let net = require('net');

let orders = [];

let currentOrder = {id: -1, items: []};

let server = net.createServer(function (c) {
    console.log('socket opened');
    c.setEncoding('utf8');
    c.on('end', function () {
        console.log('connection/socket closed');
    });
    c.on('data', function (data) {
        data = data.trim()
        let words = data.split(" ");
        switch (words[0]) {
            case "open":
                if (currentOrder.id !== -1) {
                    c.write("Another order is currently open, you need to process it first" + "\r\n")
                } else if (words[1] != null) {
                    currentOrder.id = orders.length
                    c.write("Opened order " + words[1] + " with id: " + currentOrder.id + "\r\n")
                    console.log("Add order")
                } else {
                    c.write("To open an order please state a name as well." + "\r\n")
                }
                break;
            case "add":
                if (currentOrder.id === -1) {
                    c.write("You need to open an order first." + "\r\n")
                } else if (words[1] != null) {
                    currentOrder.items[currentOrder.items.length] = words[1]
                    c.write("Item " + words[1] + " added to order " + currentOrder.id + "\r\n")
                    console.log("Add item " + words[1])
                } else {
                    c.write("You need to state name of an item to add." + "\r\n")
                }
                break;
            case "process":
                if (currentOrder.id !== -1) {
                    c.write("Order " + currentOrder.id + " has been processed, you can open another one or exit." + "\r\n")
                    console.log("Close order with " + currentOrder.items)
                    orders[orders.length] = currentOrder
                    currentOrder.items = []
                    currentOrder.id = -1
                } else {
                    c.write("Order has to be created first." + "\r\n")
                }
                break;
            default:
                c.write("Command not recognised." + "\r\n")
                break;
        }
    });
});

server.listen(8124, function () { // start server (port 8124)
    console.log('server started');
});