let net = require('net');

let finalOrders = [];

let openOrders = [];

let currentOrder = {id: -1, items: []}

let server = net.createServer(function (c) {
    console.log('socket opened');
    c.setEncoding('utf8');
    c.on('end', function () {
        console.log('connection/socket closed');
    });
    c.on('data', function (data) {
        data = data.trim()
        let words = data.split(" ");
        switch (words[0]) {
            case "open":
                if (words[1] != null) {
                    currentOrder.id = finalOrders.length + openOrders.length
                    //Need deep copy
                    openOrders[openOrders.length] = JSON.parse(JSON.stringify(currentOrder))
                    c.write("Opened order " + words[1] + " with id: " + currentOrder.id + "\r\n")
                    console.log("Open order")
                } else {
                    c.write("To open an order please state a name as well." + "\r\n")
                }
                break;
            case "add":
                if (words[1] != null && words[2] != null) {
                    let itemIndex = -1
                    for (let i = 0; i < openOrders.length; i++) {
                        if (openOrders[i].id === parseInt(words[1], 10)) {
                            itemIndex = i
                            break
                        }
                    }
                    if (itemIndex !== -1) {
                        openOrders[itemIndex].items[openOrders[itemIndex].items.length] = words[2]
                        c.write("Item " + words[2] + " added to order " + words[1] + "\r\n")
                        console.log("Add item " + words[2])
                    } else {
                        c.write("Order with id: " + words[1] + " not found" + "\r\n")
                    }
                } else {
                    c.write("You need to state name of an item to add and id of an order." + "\r\n")
                }
                break;
            case "process":
                if (words[1] != null) {
                    let itemIndex = -1
                    for (let i = 0; i < openOrders.length; i++) {
                        if (openOrders[i].id === parseInt(words[1], 10)) {
                            itemIndex = i
                            break
                        }
                    }
                    if (itemIndex !== -1) {
                        finalOrders[finalOrders.length] = JSON.parse(JSON.stringify(openOrders[itemIndex]))
                        openOrders.splice(itemIndex, 1)
                        c.write("Order " + words[1] + " has been processed." + "\r\n")
                        console.log("Close order with " + finalOrders[finalOrders.length - 1].items + "\r\n")
                    } else {
                        c.write("Order with id: " + words[1] + " not found." + "\r\n")
                    }
                } else {
                    c.write("You need to state id of the order to process." + "\r\n")
                }
                break;
            default:
                c.write("Command not recognised." + "\r\n")
                break;
        }
    });
});

server.listen(8124, function () { // start server (port 8124)
    console.log('server started');
});