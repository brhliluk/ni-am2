let http = require('http');

const server = http.createServer((req, res) => {
    res.end("Hello " + req.url.slice(1) + '\n');
});

server.listen(8080, function () {
    console.log('server started on port 8080');
});


