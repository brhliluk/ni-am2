if (window.EventSource != null) {
    const HOST = 'http://localhost:6406/';

    const source = new EventSource(HOST);

    const textArea = document.getElementById('sse-message');

    source.addEventListener('message', function (e) {
        console.log(e.data);
        textArea.value = e.data;
    }, false);

    source.addEventListener('open', function () {
        console.log('opened');
    }, false);

    source.addEventListener('error', function (e) {
        if (e.readyState === EventSource.CLOSED) {
            console.log('closed');
        }
    }, false);

} else {
    console.log('No window.EventSource');
}
