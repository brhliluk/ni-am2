const http = require('http');

const PORT = 6406;
const INTERVAL = 2000

const server = http.createServer((req, res) => {

    sendMessages(req, res);

});

function sendMessages(req, res) {
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Access-Control-Allow-Origin': '*'
    });

    let message_counter = 0;

    let interval = setInterval(() => {
        res.write('data: SSE message: ' + message_counter + '\n\n');
        message_counter++;
    }, INTERVAL);

    req.on('close', () => {
        clearInterval(interval);
    });
}

server.listen(PORT, () => {
    console.log('server listening on port', PORT);
});
